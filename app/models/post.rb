class Post < ActiveRecord::Base
 has_many :comments, dependent: :destroy
 validates_presence_of :post_name
 validates_presence_of :post_body
end
